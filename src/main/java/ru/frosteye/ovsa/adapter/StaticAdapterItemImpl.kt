package ru.frosteye.ovsa.adapter

import android.view.View
import androidx.annotation.CallSuper


abstract class StaticAdapterItemImpl<V> : AdapterItem where V : View, V : AdapterView {

    abstract override val layoutResourceId: Int

    override var isSelected: Boolean = false



    final override fun createViewHolder(view: View, adapter: MultiTypeAdapter): ModelViewHolder {
        view as V
        onViewCreated(view)
        val holder = ModelViewHolder(view, adapter)
        view.viewHolder = holder
        return holder
    }

    @CallSuper
    override fun bindViewHolder(holder: ModelViewHolder) {
        val view = holder.getModelView<V>()
        beforeBindModel(view)
        view.onSelection(isSelected)
        afterBindModel(view)
    }

    open fun onViewCreated(view: V) {

    }

    open fun beforeBindModel(view: V) {

    }

    open fun afterBindModel(view: V) {

    }
}
