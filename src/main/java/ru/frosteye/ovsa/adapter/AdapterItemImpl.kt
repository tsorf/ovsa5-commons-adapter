package ru.frosteye.ovsa.adapter

import android.view.View
import androidx.annotation.CallSuper


abstract class AdapterItemImpl<T, V>(
    val model: T
) : AdapterItem where V : View, V : AdapterModelView<T> {

    abstract override val layoutResourceId: Int

    override var isSelected: Boolean = false

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false

        val that = other as AdapterItemImpl<*, *>?

        return if (model != null) model == that?.model else that?.model == null

    }

    override fun hashCode(): Int {
        return model?.hashCode() ?: 0
    }


    final override fun createViewHolder(view: View, adapter: MultiTypeAdapter): ModelViewHolder {
        view as V
        onViewCreated(view)
        val holder = ModelViewHolder(view, adapter)
        view.viewHolder = holder
        return holder
    }

    @CallSuper
    override fun bindViewHolder(holder: ModelViewHolder) {
        val view = holder.getModelView<V>()
        beforeBindModel(view)
        view.model = model
        view.onSelection(isSelected)
        afterBindModel(view)
    }

    open fun onViewCreated(view: V) {

    }

    open fun beforeBindModel(view: V) {

    }

    open fun afterBindModel(view: V) {

    }
}
