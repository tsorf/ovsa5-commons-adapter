package ru.frosteye.ovsa.adapter

import android.util.SparseArray
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.util.contains
import androidx.core.util.containsKey
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.subjects.PublishSubject
import java.lang.IllegalStateException
import java.lang.IndexOutOfBoundsException

class MultiTypeAdapter : RecyclerView.Adapter<ModelViewHolder>(), Iterable<AdapterItem> {

    private var dataSet: ArrayList<AdapterItem> = arrayListOf()
    private var typeMap = SparseArray<AdapterItem>()

    private val eventsPublisher = PublishSubject.create<AdapterEvent>()
    val adapterEvents: Observable<AdapterEvent> = eventsPublisher

    internal fun publishEvent(event: AdapterEvent) {
        eventsPublisher.onNext(event)
    }

    private fun initializeNewItems(items: List<AdapterItem>) {
        for (it in items) {
            initializeNewItem(it)
        }
    }

    private fun initializeNewItem(item: AdapterItem) {
        if (!typeMap.containsKey(item.layoutResourceId)) {
            typeMap.put(item.layoutResourceId, item)
        }
    }

    fun updateDataSet(items: List<AdapterItem>) {
        clearData()
        dataSet = ArrayList(items)
        initializeNewItems(dataSet)
        notifyDataSetChanged()
    }

    fun snapshot(): List<AdapterItem> {
        return dataSet.toList()
    }

    fun clear() {
        clearData()
        notifyDataSetChanged()
    }

    private fun clearData() {
        dataSet = ArrayList()
        typeMap.clear()
    }

    fun addItem(item: AdapterItem, position: Int? = null) {
        val itemPosition = position ?: dataSet.size
        dataSet.add(itemPosition, item)
        initializeNewItem(item)
        notifyItemInserted(itemPosition)
    }

    fun addItems(items: List<AdapterItem>, position: Int? = null) {
        val currentSize = dataSet.size
        val itemPosition = position ?: currentSize
        dataSet.addAll(itemPosition, items)
        initializeNewItems(items)
        notifyItemRangeInserted(itemPosition, items.size)
    }

    fun updateItem(item: AdapterItem) {
        val itemPosition = dataSet.indexOf(item)
        if (itemPosition == -1) {
            throw IllegalStateException("no $item in the dataSet")
        }
        dataSet[itemPosition] = item
        notifyItemChanged(itemPosition)
    }

    /**
     * @return true, если элемент добавлен
     */
    fun addOrUpdate(item: AdapterItem): Boolean {
        val itemPosition = dataSet.indexOf(item)
        return if (itemPosition == -1) {
            addItem(item)
            true
        } else {
            dataSet[itemPosition] = item
            notifyItemChanged(itemPosition)
            false
        }
    }

    fun removeItem(item: AdapterItem) {
        val itemPosition = dataSet.indexOf(item)
        if (itemPosition == -1) {
            throw IllegalStateException("no $item in the dataSet")
        }
        dataSet.removeAt(itemPosition)
        notifyItemRemoved(itemPosition)
    }

    fun removeItemAtPosition(itemPosition: Int) {
        if (itemPosition < 0 || itemPosition >= itemCount) {
            throw IndexOutOfBoundsException("wrong position: $itemPosition")
        }
        dataSet.removeAt(itemPosition)
        notifyItemRemoved(itemPosition)
    }

    fun getItemAt(position: Int): AdapterItem? {
        return dataSet.getOrNull(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ModelViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(viewType, parent, false)
        return typeMap.get(viewType).createViewHolder(view, this)
    }

    override fun getItemViewType(position: Int): Int {
        return dataSet[position].layoutResourceId
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    override fun onBindViewHolder(holder: ModelViewHolder, position: Int) {
        val item = dataSet[position]
        item.bindViewHolder(holder)
    }

    override fun iterator(): Iterator<AdapterItem> {
        return dataSet.iterator()
    }
}

fun multiTypeAdapter(): MultiTypeAdapter {
    return MultiTypeAdapter()
}
