package ru.frosteye.ovsa.adapter

interface ModelView<Model> {

    var model: Model
}

interface AdapterView {

    var viewHolder: ModelViewHolder?

    fun onSelection(selected: Boolean) {

    }
}

interface AdapterModelView<Model> : ModelView<Model>, AdapterView {


}