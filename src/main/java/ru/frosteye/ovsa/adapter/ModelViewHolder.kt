package ru.frosteye.ovsa.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView

class ModelViewHolder(
    itemView: View,
    private val adapter: MultiTypeAdapter
) : RecyclerView.ViewHolder(itemView){

    fun <V>getModelView(): V {
        return itemView as V
    }

    fun publishEvent(event: AdapterEvent) {
        adapter.publishEvent(event)
    }
}