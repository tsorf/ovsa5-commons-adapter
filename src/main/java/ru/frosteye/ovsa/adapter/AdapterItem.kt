package ru.frosteye.ovsa.adapter

import android.view.View

interface AdapterItem {

    val layoutResourceId: Int

    var isSelected: Boolean

    fun createViewHolder(view: View, adapter: MultiTypeAdapter): ModelViewHolder

    fun bindViewHolder(holder: ModelViewHolder)
}