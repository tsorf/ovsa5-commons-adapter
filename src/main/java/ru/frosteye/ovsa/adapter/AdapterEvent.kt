package ru.frosteye.ovsa.adapter

sealed class AdapterEvent {

    data class Action(
        val actionCode: Int
    ) : AdapterEvent()

    data class ModelAction(
        val model: Any,
        val actionCode: Int
    ) : AdapterEvent()

    data class Select(
        val model: Any
    ) : AdapterEvent()
}